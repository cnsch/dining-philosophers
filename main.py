from threading import Thread, Semaphore
import colorama
import time

colorama.init(autoreset=True)
COLORS = (
    colorama.Fore.RED,
    colorama.Fore.GREEN,
    colorama.Fore.CYAN,
    colorama.Fore.BLUE,
    colorama.Fore.MAGENTA
)

NUMBER_OF_PHILOSOPHERS = 5

forks = [Semaphore()] * NUMBER_OF_PHILOSOPHERS
print_lock = Semaphore()

def space(s):
    print_lock.acquire()
    print(' ' * (s * 10), end='')

def space_end():
    print_lock.release()

def left(p):
    return p

def right(p):
    return (p + 1) % NUMBER_OF_PHILOSOPHERS

def fork_info(fid, s):
    space(s)
    print(COLORS[s] + 'Intenta tenedor: {}'.format(fid))
    space_end()


def get_forks(p):
    if (p == NUMBER_OF_PHILOSOPHERS - 1):
        fork_info(right(p), p)
        forks[right(p)].acquire()

        fork_info(left(p), p)
        forks[left(p)].acquire()
    else:
        fork_info(left(p), p)
        forks[left(p)].acquire()

        fork_info(right(p), p)
        forks[right(p)].acquire()

def put_forks(p):
    forks[left(p)].release()
    forks[right(p)].release()

def think():
    return

def eat():
    return

def philosopher_action(pid, action):
    space(pid)
    print(COLORS[pid] + 'Filósofo {}: {}'.format(pid, action))
    space_end()

def philosopher(pid, num_loops):
    for i in range(num_loops):
        philosopher_action(pid, 'Pensando')
        think()
        get_forks(pid)
        time.sleep(1)
        philosopher_action(pid, 'Comiendo')
        eat()
        put_forks(pid)
        philosopher_action(pid, 'Terminó')

def main():
    for i in range(5):
        forks[i] = Semaphore()

    p = []

    for i in range(5):
        p.append(Thread(target=philosopher, args=(i, 5)))
        p[i].start()
    
    for i in range(5):
        p[i].join()


main()